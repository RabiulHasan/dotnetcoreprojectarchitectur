﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.ViewModel
{
    public class RoleSetupViewModel
    {
        public string RoleName { get; set; }
        public string Status { get; set; }
    }
}
