﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.ViewModel
{
    public class SessionModel
    {
        public static string _user_id;
        const string SessionUserName = "_UserName";
        public SessionModel()
        {
        }
        public string USER_ID
        {
            set { _user_id = value; }
            get { return _user_id; }
        }
    }
}
