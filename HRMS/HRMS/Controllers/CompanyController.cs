﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using HRMS.Models;
using Microsoft.AspNetCore.Mvc;
using HRMS.ViewModel;
using Microsoft.AspNetCore.Http;

namespace HRMS.Controllers
{
    public class CompanyController : Controller
    {
        SessionModel objs = new SessionModel();
        private DBContextModel _db;
        public CompanyController(DBContextModel db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            try
            {
              var id= objs.USER_ID;
                return View(_db.ComInfo.ToList());
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Details(int? id)
        {
            try
            {

                var com = _db.ComInfo.Where(x => x.ComId == id).FirstOrDefault();

                return View(com);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IActionResult Logo()
        {
            return PartialView("_logo");
        }
    }
}