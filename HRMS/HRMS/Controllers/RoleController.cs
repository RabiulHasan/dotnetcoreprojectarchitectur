﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRMS.Repository;
using Microsoft.Extensions.Configuration;
using HRMS.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using HRMS.ViewModel;

namespace HRMS.Controllers
{
    public class RoleController : BaseController
    {
        private readonly IConfiguration _iconfiguration;
        private readonly SqlConnection _con;
        private DBContextModel _db;
        public RoleController(IConfiguration configuration, DBContextModel db)
        {
            _iconfiguration = configuration;
            _con = new SqlConnection(_iconfiguration.GetConnectionString("DBConnection"));
            _db = db;

        }


        public IActionResult Index()
        {
            try
            {
                ViewBag.RoleList = _db.RoleSetup.ToList();

            }
            catch (Exception ex)
            {

                Alert(ex.Message, Repository.Enums.NotificationType.error);
            }

            return View();

        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(RoleSetupViewModel roleSetup)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    RoleSetup role = new RoleSetup();
                    role.RoleName = roleSetup.RoleName;
                    role.Status = roleSetup.Status;

                    _db.RoleSetup.Add(role);
                    _db.SaveChanges();
                }
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        [HttpGet]
        public IActionResult Edit(int? id)
        {
            try
            {
                var role = _db.RoleSetup.Where(x => x.Id == id).ToList();
                var response = role.FirstOrDefault();
               // return Json(response);
                return Json(response, new Newtonsoft.Json.JsonSerializerSettings());
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        [HttpDelete]
        public IActionResult Delete(int? Id)
        {
            try
            {
                var role = _db.RoleSetup.Where(x => x.Id == Id).FirstOrDefault();
                if (role != null)
                {
                    _db.RoleSetup.Remove(role);
                    _db.SaveChanges();
                }

                return RedirectToAction(nameof(Index));

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}