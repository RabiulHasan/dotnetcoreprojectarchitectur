﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using HRMS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace HRMS.Controllers
{
    public class UserController : Controller
    {
        private readonly IConfiguration _iconfiguration;
        private readonly SqlConnection _con;
        private DBContextModel _db;
        public UserController(IConfiguration configuration, DBContextModel db)
        {
            _iconfiguration = configuration;
            _con = new SqlConnection(_iconfiguration.GetConnectionString("DBConnection"));
            _db = db;

        }
        public IActionResult Index()
        {
            try
            {
                return View(_db.UserInfo.ToList());
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(UserInfo userInfo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UserInfo user = new UserInfo();
                    user.UserId = userInfo.UserId;
                    user.FullName = userInfo.FullName;
                    user.RoleId = userInfo.RoleId;
                    user.ComId = userInfo.ComId;
                    user.Status = "Pending";
                    _db.UserInfo.Add(user);
                    _db.SaveChanges();
                }
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IActionResult getRole()
        {
            ViewData["RoleId"] = new SelectList(_db.RoleSetup, "Id", "RoleName");
            return PartialView("_RoleId");
           
        }

        [HttpGet]
        public IActionResult Details(string uid)
        {
            try
            {
                var userdetails = _db.UserInfo.Where(x => x.UserId == uid).FirstOrDefault();
                return View(userdetails);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}