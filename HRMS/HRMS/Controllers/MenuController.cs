﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HRMS.Models;
using HRMS.Repository;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HRMS.Controllers
{
    public class MenuController : Controller
    {
        private readonly IConfiguration _iconfiguration;
        private readonly SqlConnection _con;
        private DBContextModel _db;
        private CommonR _objr;
        RoleSetupR objRole;
        public MenuController(IConfiguration configuration, DBContextModel db)
        {
            _iconfiguration = configuration;
            _con = new SqlConnection(_iconfiguration.GetConnectionString("DBConnection"));
            _db = db;
            // _objr = objr;
            // objRole = or;

        }

        public IActionResult Index()
        {
            try
            {
                return View(_db.MenuSetup.ToList());
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public IActionResult MenuPermission()
        {
            //RoleSetupR objRole = new RoleSetupR();
            //var roleList = new Tuple<IEnumerable<SelectListItem>>
            //        (objRole.getAllRole());
            //return View(roleList);

            ViewData["RoleId"] = new SelectList(_db.RoleSetup, "Id", "RoleName");
            return View();

        }
        [HttpPost]
        public JsonResult LoadMenu(int? Id)
        {
            int roleid = 1;
            try
            {
                List<MenuSetup> menu = new List<MenuSetup>();
                SqlCommand cmd = new SqlCommand("Select *from MenuSetup m inner join MenuPermission p on m.M_ID=p.MenuId where p.RoleId='" + roleid + "' and p.is_allow='Y'", _con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    menu.Add(new MenuSetup
                    {
                        M_ID = Convert.ToInt32(dr["M_ID"]),
                        M_P_ID = Convert.ToInt32(dr["M_P_ID"]),
                        M_NAME = Convert.ToString(dr["M_NAME"]),
                        CONTROLLER_NAME = Convert.ToString(dr["CONTROLLER_NAME"]),
                        ACTION_NAME = Convert.ToString(dr["ACTION_NAME"]),
                        Area = Convert.ToString(dr["Area"]),

                    });
                }
                ViewBag.Menulist = menu;
                return Json(menu);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [HttpPost]
        public IActionResult MenuPermission(MenuPermission menu)
        {
            return View();
        }

        [HttpPost]
        public IActionResult Delete()
        {

            return View();
        }

    }
}