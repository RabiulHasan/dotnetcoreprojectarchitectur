﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using HRMS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HRMS.Controllers
{
    public class EmailController : Controller
    {
        private readonly IConfiguration _iconfiguration;
        private readonly SqlConnection _con;
        private DBContextModel _db;
        public EmailController(IConfiguration configuration, DBContextModel db)
        {
            _iconfiguration = configuration;
            _con = new SqlConnection(_iconfiguration.GetConnectionString("DBConnection"));
            _db = db;

        }
        public IActionResult Index()
        {
            ViewBag.EmailList = _db.EmailSetup.ToList();
            return View();
        }
        [HttpPost]
        public IActionResult AddEdit(EmailSetup email,string id)
        {
            try
            {
                if (id==null && id=="")
                {
                    if (ModelState.IsValid)
                    {
                        _db.EmailSetup.Add(email);
                        _db.SaveChanges();
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                return View();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                var role = _db.EmailSetup.Where(x => x.Email== id).ToList();
                var response = role.FirstOrDefault();
                return Json(response, new Newtonsoft.Json.JsonSerializerSettings());

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}