﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRMS.ViewModel;
using HRMS.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HRMS.Controllers
{
    public class LogController : Controller
    {
        SessionModel objs = new SessionModel();
        private DBContextModel _db;
        public LogController(DBContextModel db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        //string password
        public IActionResult Login(string userId, string password)
        {
            try
            {
                var user = _db.UserInfo.Where(m => m.UserId == userId && m.Password == password && m.Status == "Active").FirstOrDefault();
                if (user != null)
                {
                    objs.USER_ID = user.UserId.ToString();
                    //obj.Email = user.Email.ToString();
                    //objs.USER_ID = user.FullName.ToString();
                    HttpContext.Session.SetString("userName", user.FullName.ToString());
                    ViewBag.UserName = user.FullName.ToString();
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {
                    ViewBag.Message = "Login Faild! Try Again.";
                }

                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
    }
}