﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.Models
{
    public class DBContextModel : DbContext
    {
        public DBContextModel(DbContextOptions<DBContextModel> options) : base(options)
        {
        }

        public DbSet<RoleSetup> RoleSetup { get; set; }
        public DbSet<MenuSetup> MenuSetup { get; set; }
        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<EmailSetup> EmailSetup { get; set; }
        public DbSet<ComInfo> ComInfo { get; set; }
    }
}
