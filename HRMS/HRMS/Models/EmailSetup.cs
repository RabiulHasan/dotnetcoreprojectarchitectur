﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.Models
{
    public class EmailSetup
    {
        //[Key, Column(Order = 0)]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int Id { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        public string Email { get; set; }
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string Password { get; set; }
        public string AddDate { get; set; }
        public string Status { get; set; }
        public string AddedBy { get; set; }


    }
}
