﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.Models
{
    public class UserInfo
    {
        [Key,Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string EmployeeId { get; set; }
        [Display(Name ="Role")]
        public int RoleId { get; set; }
        [Display(Name = "Company")]
        public int ComId { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string AddDate { get; set; }
        public string EditDate { get; set; }
        public string AddedBy { get; set; }
        public string Status { get; set; }
        public string ApprovedBy { get; set; }
    }
}
