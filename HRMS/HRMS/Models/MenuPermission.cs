﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.Models
{
    public class MenuPermission
    {
        [Key]
        public int Id { get; set; }
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public int RoleId { get; set; }    
        public string is_allow { get; set; }
    }
}
