﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.Models
{
    public class ComInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ComId { get; set; }
        public string ComName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Location { get; set; }
        public string Logo { get; set; }
        public string Slogan { get; set; }
        public string Adddate { get; set; }
        public string AddedBy { get; set; }
        public string Status { get; set; }
    }
}
