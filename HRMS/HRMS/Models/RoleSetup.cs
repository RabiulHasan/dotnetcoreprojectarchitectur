﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.Models
{
    public class RoleSetup
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RoleName { get; set; }
        public string Status { get; set; }
    }
}
