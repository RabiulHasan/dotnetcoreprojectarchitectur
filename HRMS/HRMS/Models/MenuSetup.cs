﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRMS.Models
{
    public class MenuSetup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int M_ID { get; set; }
        public int M_P_ID { get; set; }
        public string M_NAME { get; set; }
        public string CONTROLLER_NAME { get; set; }
        public string ACTION_NAME { get; set; }
        public string Module { get; set; }
        public string Status { get; set; }
        public string Area { get; set; }
    }

    //public class Menu : ViewComponent
    //{
    //    public IViewComponentResult Invoke()
    //    {
    //        return View();
    //    }
    //}
}
