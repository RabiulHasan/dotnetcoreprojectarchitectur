﻿using HRMS.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using HRMS.Repository;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace HRMS.ViewComponents
{
    public class UserMenuViewComponent : ViewComponent
    {

        private readonly IConfiguration _iconfiguration;
        private readonly SqlConnection _con;
        private DBContextModel _db;

        public UserMenuViewComponent(IConfiguration configuration)
        {
            _iconfiguration = configuration;
            _con = new SqlConnection(_iconfiguration.GetConnectionString("DBConnection"));

        }
        public IViewComponentResult Invoke()
        {

            int urole = 1;
            try
            {
                List<MenuSetup> menu = new List<MenuSetup>();
                SqlCommand cmd = new SqlCommand("Select *from MenuSetup m inner join MenuPermission p on m.M_ID=p.MenuId where p.RoleId='" + urole + "' and p.is_allow='Y'", _con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    menu.Add(new MenuSetup
                    {
                        M_ID = Convert.ToInt32(dr["M_ID"]),
                        M_P_ID = Convert.ToInt32(dr["M_P_ID"]),
                        M_NAME = Convert.ToString(dr["M_NAME"]),
                        CONTROLLER_NAME = Convert.ToString(dr["CONTROLLER_NAME"]),
                        ACTION_NAME = Convert.ToString(dr["ACTION_NAME"]),
                        Area = Convert.ToString(dr["Area"]),

                    });
                }
                return View(menu);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
