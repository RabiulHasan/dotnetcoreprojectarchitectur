﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using HRMS.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace HRMS.Repository
{
    public class RoleSetupR
    {
        private readonly IConfiguration _iconfiguration;
        private readonly SqlConnection _con;
        private DBContextModel _db;

        public RoleSetupR(IConfiguration configuration, DBContextModel db)
        {
            _iconfiguration = configuration;
            _con = new SqlConnection(_iconfiguration.GetConnectionString("DBConnection"));
            _db = db;

        }
        public IEnumerable<SelectListItem> getAllRole()
        {
            var objselectListItems = new List<SelectListItem>();
            objselectListItems = (from obj in _db.RoleSetup
                                  select new SelectListItem
                                  {
                                      Text = obj.RoleName,
                                      Value = obj.Id.ToString(),
                                      // Selected = true
                                  }).GroupBy(n => n.Text)
                                           .Select(g => g.FirstOrDefault())
                                           .ToList();
            return objselectListItems;
        }
    }
}
