﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using HRMS.Models;
using Microsoft.Extensions.Configuration;

namespace HRMS.Repository
{
    public class CommonR
    {
        private readonly IConfiguration _iconfiguration;
        private readonly SqlConnection _con;
        private DBContextModel _db;
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        public CommonR()
        {
        }
        public CommonR(IConfiguration configuration)
        {
            _iconfiguration = configuration;
            _con = new SqlConnection(_iconfiguration.GetConnectionString("DBConnection"));

        }

        public DataTable FetchData(string query)
        {
            try
            {
                _con.Open();
                da = new SqlDataAdapter(query, _con);
                dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
        public void MenuSetup()
        {
            string tblname = "MenuSetup";
            string excSql;
            string query = "SELECT * FROM sysobjects WHERE (name LIKE 'MenuSetup' )";
            cmd = new SqlCommand(query);
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                if (dr.HasRows == false)
                {
                    excSql = "CREATE TABLE [dbo].[" + tblname + "] ( "
                + "	[M_ID] [int] PRIMARY KEY  NOT NULL,  "
                + "	[M_P_ID] [int] NOT NULL, "
                + "	[M_NAME] [nvarchar](255) NULL , "
                + "	[CONTROLLER_NAME] [nvarchar](100) NULL , "
                + "	[ACTION_NAME] [nchar](100) NULL , "
                + "	[Module] [nvarchar](50) NULL , "
                + "	[Status] [nvarchar](10) NULL,"
                + "	[Area] [nvarchar](50) NULL"
                + " ) ON [PRIMARY] ";
                    cmd = new SqlCommand(excSql);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
